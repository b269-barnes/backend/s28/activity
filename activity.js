db.rooms.insertOne({
	name: "single",
	accomodates: 2,
	price :  1000,
	description : "A simple room with all the basic necessities",
	rooms_available : 10,
	isAvailable : "false"
});

db.rooms.insertMany([
{
	name: "double",
	accomodates : 3,
	price : 2000,
	description : "A room fit for a small family going on a vacation",
	rooms_available : 5,
	inAvailable: "false"
},
{
	name: "queen",
	accomodates : 4,
	price : 4000,
	description : "A room with a queen sized bed perfect for a simple getaway",
	rooms_available : 15,
	inAvailable: "false"
}
]);

db.rooms.find();

db.rooms.updateOne(
		{
		name: "queen",
			accomodates : 4,
			price : 4000,
			description : "A room with a queen sized bed perfect for a simple getaway",
			rooms_available : 15,
			inAvailable: "false"
		},
		{
			$set: {
				rooms_available : 0
			}
		}


	);
	
db.rooms.deleteMany({
	rooms_available: 0
});

